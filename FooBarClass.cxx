#include "FooBarClass.h"
#include <stdio.h>
#include <process.h>

FooBarClass::FooBarClass(QObject *parent) :
    QObject(parent)
{
	m_input1 = "";
	m_input2 = "";
	m_output = "";
}

void FooBarClass::SetInput1(QString input1)
{
    m_input1 = input1;
}

void FooBarClass::SetInput2(QString input2)
{
    m_input2 = input2;
}

QString FooBarClass::GetInput1()
{
    return m_input1;
}

QString FooBarClass::GetInput2()
{
    return m_input2;
}

QString FooBarClass::GetOutput()
{
    return m_output;
}

void FooBarClass::Compute()
{
	m_output = m_input1 + " " + m_input2;
}

int FooBarClass::Update()
{
	emit Process(50);
	this->Compute();
	emit Finished();
	return 0;
}


#include "FooBarClass.h"
#include <QDebug>

int main()
{
  FooBarClass* fooBarClass = new FooBarClass;
  fooBarClass->SetInput1("Hello");
  fooBarClass->SetInput2("World");
  fooBarClass->Update();

  qDebug()<<fooBarClass->GetOutput();

  system("pause");
  delete fooBarClass;
  return 0;
}
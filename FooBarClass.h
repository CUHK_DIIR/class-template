#ifndef FOOBARCLASS_H
#define FOOBARCLASS_H

#include <QObject>

class FooBarClass: public QObject
{
    Q_OBJECT
public:
    	explicit FooBarClass(QObject *parent = 0);
    	void SetInput1(QString input1);
    	void SetInput2(QString input2);
	
	QString GetInput1();	
	QString GetInput2();	
	QString GetOutput();

signals:
	void Process(int);
	void Finished();

public slots:
	int Update();

private:
	void Compute();

	QString m_input1;
	QString m_input2;
	QString m_output;
};

#endif // FOOBARCLASS_H
